




// BÀI 1: TÌM N
function timN() {
    var count = 1;
    var sum = 0;
    var temp = 0;

    for (count = 1; count < 10000; count++) {
        sum += count;
        if ((sum) > 10000) {
            temp = count;
            break;
        }
    }
    document.getElementById("txt-kq").innerHTML = `Số n cần tìm là: ${temp}`;
}
// BÀI 2: TÍNH TỔNG S(N)
function tinhTongXN() {
    var xEl = document.getElementById("num-x").value * 1;
    var nEl = document.getElementById("num-n").value * 1;
    var i = 0;
    var El = 0;
    var sum = 0;
    for (i = 0; i <= nEl; i++) {
        El = xEl ** i;
        sum += El;
    }
    document.getElementById("txt-2-result").innerHTML = `Tổng S(n) cần tìm là: ${sum}`;
}
// BÀI 3: TÍNH GIAI THỪA
function giaiThua() {
    var n = document.getElementById("num-3-n").value * 1;
    var i = 1;
    var giaiThua = 1;
    for (i = 1; i <= n; i++) {
        giaiThua = giaiThua * i;
    }
    document.getElementById("txt-3-result").innerHTML = `${n}!: ${giaiThua}`;
}
// BÀI 4: TẠO ĐÍP
function taoDiv() {
    var count = 0;
    var El = "";
    var divEl = "";
    for (let count = 0; count <= 10; count++) {
        count % 2 == 0 ? El = "<div class='bg-danger text-white p-2'>Div chẵn</div>" : El = "<div class='bg-primary text-white  p-2'>Div lẻ</div>"
        divEl += El;
    }
    document.getElementById("txt-4-result").innerHTML = divEl;
}
// BÀI 5: IN DÃY SỐ NGUYÊN TỐ
function checkPrime(a) {
    if (a < 2) {
        return false;
    } else if (a == 2) {
        return true;
    }
    else if (a % 2 == 0) {
        return false;
    }
    else {
        for (let i = 3; i <= Math.sqrt(a); i += 2) {
            if (a % i == 0) {
                return false;
            }
        }
    } return true;

}
function inNguyenTo() {
    var n = document.getElementById("num-5-n").value * 1;
    var primeStr = ""
    for (i = 2; i <= n; i++) {
        if (checkPrime(i)) {
            primeStr = primeStr + i + " ";
        }
    }
    document.getElementById("txt-5-result").innerHTML = primeStr;
}
